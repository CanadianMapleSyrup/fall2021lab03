package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
/**
 * Nolan Ganz 2038533
 */
public class Vector3dTest {
    
    @Test
    public void getXTest()
    {
        Vector3d v1 = new Vector3d(1,2,3);
        assertEquals(1,v1.getX());
    }

    @Test
    public void getYTest()
    {
        Vector3d v1 = new Vector3d(1,2,3);
        assertEquals(2,v1.getY());
    }

    @Test
    public void getZTest()
    {
        Vector3d v1 = new Vector3d(1,2,3);
        assertEquals(3,v1.getZ());
    }

    @Test
    public void magnitudeTest()
    {
        Vector3d v1 = new Vector3d(1,2,3);
        assertEquals(3.741, v1.magnitude(), 0.1);
    }

    @Test
    public void dotProductTest()
    {
        Vector3d v1 = new Vector3d(1,2,3);
        Vector3d v2 = new Vector3d(4,5,6);
        assertEquals(32, v1.dotProduct(v2));
    }

    @Test
    public void addTest()
    {
        Vector3d v1 = new Vector3d(1,2,3);
        Vector3d v2 = new Vector3d(4,5,6);
        Vector3d addedVector = v1.add(v2);
        assertEquals(5,addedVector.getX());
        assertEquals(7,addedVector.getY());
        assertEquals(9,addedVector.getZ());
    }


}
